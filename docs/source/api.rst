.. _api:

API Reference
=============

.. module:: diana

Injector
--------

.. autoclass:: diana.injector.Injector
   :member-order: bysource
   :members:


Scopes
------

.. autoclass:: diana.scopes.Scope
.. autoclass:: diana.scopes.Const


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

